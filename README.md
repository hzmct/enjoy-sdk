# EnjoySDK 

## 项目介绍
EnjoySDK 是基于杭州迈冲科技 Android 智能设备系统开发的一套方便客户进行二次开发的 SDK。拥有丰富的 Android 硬件控制接口，满足行业应用开发。

EnjoySDK 必须在集成了对应版本 EnjoyApi 的 Android 系统中才能生效，

## 技术特性
**接口丰富性**  
EnjoySDK 在 Android 原生的基础上增加了丰富的硬件控制接口模块供用户进行二次开发。

**接口安全性**  
EnjoySDK 安全权限功能给用户提供了安全控制接口，保证接口使用的安全性。

## 开发指南
使用文档和更新说明见 [EnjoySDK 开发指南](https://alidocs.dingtalk.com/i/p/R2PmKb7Dp3WajXvp)。

## 下载
源码中的 Release 文件夹中包含最新版本和历史版本的 EnjoySDK aar, 用户可自行选择下载体验。

建议使用最新版本，接口更丰富，使用更简洁。

>  注意：请根据产品型号正确选择 V1 和 V2 版本 aar。详见说明文档。

## 鸣谢
感谢杭州迈冲科技各位工程师的辛苦付出。

感谢各位用户对 aar 不足之处的指点和建议。

万分感谢 🙏。