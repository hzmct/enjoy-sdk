package com.mc.enjoy.module;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.mc.android.mckeepalive.McKeepAliveApp;
import com.mc.enjoy.R;
import com.mc.enjoy.ToastUtils;
import com.mc.enjoysdk.McKeepAlive;
import com.mc.enjoysdk.result.McResultBool;
import com.mc.enjoysdk.transform.McErrorCode;

import java.util.ArrayList;

public class KeepAliveActivity extends AppCompatActivity {
    private Switch keepalive_switch, keepaliveafterlauncher, keepalive_process;
    private EditText keepalive_packagename, keepalive_intervaltime, keepalive_priority;
    private TextView keepalive_info, currentkeepaliveapp;
    private McKeepAlive mcKeepAlive;
    private boolean backprocess = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keep_alive);

        mcKeepAlive = McKeepAlive.getInstance(this);
        initview();
        initdate();
    }

    private void initview(){
        keepalive_switch = findViewById(R.id.keepalive_switch);
        keepaliveafterlauncher = findViewById(R.id.keepaliveafterlauncher);
        keepalive_process = findViewById(R.id.keepalive_process);
        keepalive_packagename = findViewById(R.id.keepalive_packagename);
        keepalive_intervaltime = findViewById(R.id.keepalive_intervaltime);
        keepalive_priority = findViewById(R.id.keepalive_priority);
        keepalive_info = findViewById(R.id.keepalive_info);
        currentkeepaliveapp = findViewById(R.id.currentkeepaliveapp);

        keepalive_switch.setChecked(mcKeepAlive.isKeepAliveOpen() == McResultBool.TRUE);
        keepaliveafterlauncher.setChecked(mcKeepAlive.isKeepAliveAfterAppStart() == McResultBool.TRUE);

        keepalive_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                mcKeepAlive.EnableKeepAlive(b);
                if(b) currentKeepAliveInfo();
            }
        });

        keepaliveafterlauncher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                mcKeepAlive.setKeepAliveAfterAppStart(b);
                if(b) currentKeepAliveInfo();
            }
        });

        keepalive_process.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                backprocess = b;
            }
        });
    }

    private void initdate(){
        if(keepalive_switch.isChecked()){
            allKeepAliveInfo();
        }
    }

    private void currentKeepAliveInfo(){
        McKeepAliveApp mcKeepAliveApp = mcKeepAlive.getCurrentKeepAliveAppInfo();
        String info = "当前保活应用信息：\n";
        if(mcKeepAliveApp == null){
            info += "null";
        }else{
            info += "应用包名："+mcKeepAliveApp.getPackagename()+"\n"+
                    "时间间隔："+mcKeepAliveApp.getInterval()+"\n"+
                    "保活优先级"+mcKeepAliveApp.getPriority()+"\n"+
                    "保活方式："+(mcKeepAliveApp.isBackstage()?"进程":"activity")+"保活\n";
        }
        keepalive_info.setText(info);
        updatecurrentKeepalive(mcKeepAlive.getCurrentKeepAliveAppInfo());
    }

    private void allKeepAliveInfo(){
        ArrayList<McKeepAliveApp> list = mcKeepAlive.getKeepAliveAPP();
        String info = "所有保活应用信息：";
        if(list == null){
            info += "\nnull";
        }else{
            for(McKeepAliveApp mc: list){
                info += "\n应用包名："+mc.getPackagename()+"\n"+
                        "时间间隔："+mc.getInterval()+"\n"+
                        "保活优先级"+mc.getPriority()+"\n"+
                        "保活方式："+(mc.isBackstage()?"进程":"activity")+"保活\n";
            }
        }
        keepalive_info.setText(info);
        updatecurrentKeepalive(mcKeepAlive.getCurrentKeepAliveAppInfo());
    }

    private void updatecurrentKeepalive(McKeepAliveApp mcKeepAliveApp){
        String result = "当前保活应用：";
        if(mcKeepAliveApp == null){
            result += "null";
        }else{
            result += mcKeepAliveApp.getPackagename();
        }
        currentkeepaliveapp.setText(result);
    }

    private void SpecialKeepAliveInfo(String packagename){
        McKeepAliveApp mcKeepAliveApp = mcKeepAlive.getKeepAliveAppInfo(packagename);
        String info = packagename+" 保活信息：\n";
        if(mcKeepAliveApp == null){
            info += "null";
        }else{
            info += "应用包名："+mcKeepAliveApp.getPackagename()+"\n"+
                    "时间间隔："+mcKeepAliveApp.getInterval()+"\n"+
                    "保活优先级"+mcKeepAliveApp.getPriority()+"\n"+
                    "保活方式："+(mcKeepAliveApp.isBackstage()?"进程":"activity")+"保活\n";
        }
        keepalive_info.setText(info);
    }

    public void addKeepAlive(View view) {
        String packagename = keepalive_packagename.getText().toString().trim();
        if(TextUtils.isEmpty(packagename)){
            ToastUtils.showShort(KeepAliveActivity.this,"请输入包名");
            return;
        }
        int interval;
        try{
            interval = Integer.parseInt(keepalive_intervaltime.getText().toString());
        }catch (Exception e){
            interval = 5;
            ToastUtils.showShort(KeepAliveActivity.this,"默认保活时间间隔为5秒");
        }
        int priority;
        try{
            priority = Integer.parseInt(keepalive_priority.getText().toString());
        }catch (Exception e){
            ToastUtils.showShort(KeepAliveActivity.this,"请输入保活优先级");
            return;
        }
        ArrayList<McKeepAliveApp> list = new ArrayList<McKeepAliveApp>();
        McKeepAliveApp mcKeepAliveApp = new McKeepAliveApp();
        mcKeepAliveApp.setPackagename(packagename);
        mcKeepAliveApp.setInterval(interval);
        mcKeepAliveApp.setBackstage(backprocess);
        mcKeepAliveApp.setPriority(priority);
        list.add(mcKeepAliveApp);
        int ret = mcKeepAlive.addKeepAliveAPP(list);
        parseError(ret);
        allKeepAliveInfo();
    }

    public void removeKeepAlive(View view) {
        String packagename = keepalive_packagename.getText().toString().trim();
        if(TextUtils.isEmpty(packagename)){
            ToastUtils.showShort(KeepAliveActivity.this,"请输入包名");
            return;
        }
        ArrayList<McKeepAliveApp> list = new ArrayList<McKeepAliveApp>();
        McKeepAliveApp mcKeepAliveApp = new McKeepAliveApp();
        mcKeepAliveApp.setPackagename(packagename);
        list.add(mcKeepAliveApp);
        int ret = mcKeepAlive.removeKeepAliveAPP(list);
        parseError(ret);
        allKeepAliveInfo();
    }

    public void updateKeepAlive(View view) {
        String packagename = keepalive_packagename.getText().toString().trim();
        if(TextUtils.isEmpty(packagename)){
            ToastUtils.showShort(KeepAliveActivity.this,"请输入包名");
            return;
        }
        int interval;
        try{
            interval = Integer.parseInt(keepalive_intervaltime.getText().toString());
        }catch (Exception e){
            interval = 5;
            ToastUtils.showShort(KeepAliveActivity.this,"默认保活时间间隔为5秒");
        }
        int priority;
        try{
            priority = Integer.parseInt(keepalive_priority.getText().toString());
        }catch (Exception e){
            ToastUtils.showShort(KeepAliveActivity.this,"请输入保活优先级");
            return;
        }
        McKeepAliveApp mcKeepAliveApp = new McKeepAliveApp();
        mcKeepAliveApp.setPackagename(packagename);
        mcKeepAliveApp.setInterval(interval);
        mcKeepAliveApp.setBackstage(backprocess);
        mcKeepAliveApp.setPriority(priority);
        int ret = mcKeepAlive.setKeepAliveAppPreference(mcKeepAliveApp);
        parseError(ret);
        allKeepAliveInfo();
    }

    public void AppisKeepAlive(View view) {
        String packagename = keepalive_packagename.getText().toString().trim();
        if(TextUtils.isEmpty(packagename)){
            ToastUtils.showShort(KeepAliveActivity.this,"请输入包名");
            return;
        }
        int ret = mcKeepAlive.isKeepAliveApp(packagename);
        parseError(ret);
    }

    public void getAllKeepAlive(View view) {
        allKeepAliveInfo();
    }

    public void getSpecialKeepAlive(View view) {
        String packagename = keepalive_packagename.getText().toString().trim();
        if(TextUtils.isEmpty(packagename)){
            ToastUtils.showShort(KeepAliveActivity.this,"请输入包名");
            return;
        }
        SpecialKeepAliveInfo(packagename);
    }

    private void parseError(int errorCode) {
        switch (errorCode) {
            case McErrorCode.ENJOY_COMMON_SUCCESSFUL:
                ToastUtils.showShort(KeepAliveActivity.this, "成功");
                break;
            case McErrorCode.ENJOY_COMMON_ERROR_SERVICE_NOT_START:
                ToastUtils.showShort(KeepAliveActivity.this, "应用保活服务未启动："+McErrorCode.errorCode2Str(errorCode));
                break;
            case McErrorCode.ENJOY_KEEP_ALIVE_MANAGER_TIME_INFO_ERROR:
                ToastUtils.showShort(KeepAliveActivity.this, "APP 保活信息错误："+McErrorCode.errorCode2Str(errorCode));
                break;
            case McErrorCode.ENJOY_KEEP_ALIVE_MANAGER_IS_KEEP_ALIVE:
                ToastUtils.showShort(KeepAliveActivity.this,keepalive_packagename.getText().toString()+" 在保活应用列表内");
                break;
            case McErrorCode.ENJOY_KEEP_ALIVE_MANAGER_NOT_KEEP_ALIVE:
                ToastUtils.showShort(KeepAliveActivity.this,keepalive_packagename.getText().toString()+" 不在保活应用列表内");
                break;
            case McErrorCode.ENJOY_KEEP_ALIVE_MANAGER_ERROR_WHITE_ADD:
                ToastUtils.showShort(KeepAliveActivity.this, "添加保活应用信息操作错误："+McErrorCode.errorCode2Str(errorCode));
                break;
            case McErrorCode.ENJOY_KEEP_ALIVE_MANAGER_ERROR_WHITE_REMOVE:
                ToastUtils.showShort(KeepAliveActivity.this, "删除保活应用信息操作错误："+McErrorCode.errorCode2Str(errorCode));
                break;
            case McErrorCode.ENJOY_KEEP_ALIVE_MANAGER_UPDATE_FAILED:
                ToastUtils.showShort(KeepAliveActivity.this, "应用保活信息更新失败："+McErrorCode.errorCode2Str(errorCode));
                break;
            default:
                ToastUtils.showShort(KeepAliveActivity.this, "未知错误："+McErrorCode.errorCode2Str(errorCode));
                break;
        }
    }
}