package com.mc.enjoy.module;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.mc.enjoy.R;
import com.mc.enjoy.ToastUtils;
import com.mc.enjoysdk.McFirmwareInfo;
import com.mc.enjoysdk.result.McResultBool;
import com.mc.enjoysdk.util.PropUtil;

public class FirmwareActivity extends AppCompatActivity {
    private static final String TAG = "FirmwareActivity";

    TextView tvFactory;
    TextView tvProduct;
    TextView tvSpecial;
    TextView tvCpuType;
    TextView tvAndroidVersion;
    TextView tvFirmwareVersion;
    TextView tvFirmwareCode;
    EditText serialnumber;

    private McFirmwareInfo mcFirmwareInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firmware);

        tvFactory = findViewById(R.id.tv_factory);
        tvProduct = findViewById(R.id.tv_product);
        tvSpecial = findViewById(R.id.tv_special);
        tvCpuType = findViewById(R.id.tv_cpu_type);
        tvAndroidVersion = findViewById(R.id.tv_android_version);
        tvFirmwareVersion = findViewById(R.id.tv_firmware_version);
        tvFirmwareCode = findViewById(R.id.tv_firmware_code);
        serialnumber = findViewById(R.id.serialnumber);

        mcFirmwareInfo = McFirmwareInfo.getInstance(this);

        tvFactory.setText(String.format(getResources().getString(R.string.factory), mcFirmwareInfo.getFactoryInfo()));
        tvProduct.setText(String.format(getResources().getString(R.string.product), mcFirmwareInfo.getProductInfo()));
        tvSpecial.setText(String.format(getResources().getString(R.string.special), mcFirmwareInfo.getSpecialInfo()));
        tvCpuType.setText(String.format(getResources().getString(R.string.cpu_type), mcFirmwareInfo.getCpuTypeInfo()));
        tvAndroidVersion.setText(String.format(getResources().getString(R.string.android_version), mcFirmwareInfo.getAndroidVersionInfo()));
        tvFirmwareVersion.setText(String.format(getResources().getString(R.string.firmware_version), mcFirmwareInfo.getFirmwareVersion()));
        tvFirmwareCode.setText(String.format(getResources().getString(R.string.firmware_code), mcFirmwareInfo.getFirmwareVersionCode()));
        String SN = PropUtil.getStringFromProp("persist.mc.clouduserid");
        if(TextUtils.isEmpty(SN)){
            SN = "";
        }
        serialnumber.setText(SN);
    }

    public void writeSN(View view) {
        String sn = serialnumber.getText().toString().trim();
        if(TextUtils.isEmpty(sn)){
            ToastUtils.showShort(FirmwareActivity.this, "请输入SN号");
            return;
        }
        if(mcFirmwareInfo.setSerialNo(sn) == McResultBool.TRUE){
            ToastUtils.showShort(FirmwareActivity.this, "写入成功");
        }else{
            ToastUtils.showShort(FirmwareActivity.this, "写入失败");
        }
    }
}