package com.mc.enjoy.module;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.mc.enjoy.R;
import com.mc.enjoy.ToastUtils;
import com.mc.enjoysdk.McPower;
import com.mc.enjoysdk.transform.McErrorCode;
import com.mc.enjoysdk.transform.McPowerFlag;

/**
 * @author Woong on 1/27/21
 * @website http://woong.cn
 */
public class PowerActivity extends AppCompatActivity {
    private static final String TAG = "PowerActivity";

    private Button btnReboot;
    private Button btnDialogShutdown;
    private Button btnShutdown;
    private Button btnBrightness;
    private Button btnTouchWake;
    private Button btnTouchStatu;
    private EditText brightnessvalue;
    private int touch_wake_state,timed_wake_switch_state;
    private Switch timed_wake_switch;
    private Button add_time_touch_wake, delete_time_touch_wake;
    private EditText add_time_touch_wake_value, delete_time_touch_wake_value;
    private Button cancle_all_time_touch_plan, get_time_touch_switch_state, get_time_touch_state;
    private Switch manual_config_battery, battery_waring;
    private Button update_battery_level, update_battery_state, update_battery_thread, update_battery_waring;
    private EditText battery_level_value, battery_thread_value;
    private Spinner battery_state_value;
    private int manual_config_battery_state_value = 0;

    private McPower mcPower;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_power);

        btnReboot = findViewById(R.id.btn_reboot);
        btnDialogShutdown = findViewById(R.id.btn_dialog_shutdown);
        btnShutdown = findViewById(R.id.btn_shutdown);
        btnBrightness = findViewById(R.id.btn_set_brightness);
        btnTouchWake = findViewById(R.id.btn_touch_wake);
        btnTouchStatu = findViewById(R.id.btn_touch_statu);
        brightnessvalue = findViewById(R.id.brightness_value);
        timed_wake_switch = findViewById(R.id.timed_touch_wake_switch);
        add_time_touch_wake = findViewById(R.id.add_time_touch_wake);
        add_time_touch_wake_value = findViewById(R.id.add_time_touch_wake_value);
        delete_time_touch_wake = findViewById(R.id.delete_time_touch_wake);
        delete_time_touch_wake_value = findViewById(R.id.delete_time_touch_wake_value);
        cancle_all_time_touch_plan = findViewById(R.id.cancle_all_time_touch_plan);
        get_time_touch_switch_state = findViewById(R.id.get_time_touch_switch_state);
        get_time_touch_state = findViewById(R.id.get_time_touch_state);
        manual_config_battery = findViewById(R.id.manual_config_battery);
        battery_waring = findViewById(R.id.battery_waring_value);
        update_battery_level = findViewById(R.id.update_battery_level);
        update_battery_state = findViewById(R.id.update_battery_state);
        update_battery_thread = findViewById(R.id.update_battery_threadvalue);
        update_battery_waring = findViewById(R.id.update_battery_waring);
        battery_level_value = findViewById(R.id.battery_level_value);
        battery_state_value = findViewById(R.id.battery_state_value);
        battery_thread_value = findViewById(R.id.battery_threadvalue_value);

        mcPower = McPower.getInstance(this);
        touch_wake_state = mcPower.getTouchWakeStatu();
        btnTouchWake.setText(
                (touch_wake_state== McPowerFlag.TOUCH_WAKE_ENABLED)?
                        "打开触摸唤醒":"关闭触摸唤醒");
        timed_wake_switch_state = mcPower.getTimedTouchWakeState();
        if(timed_wake_switch_state != McErrorCode.ENJOY_COMMON_ERROR_DEVICE_NOT_SUPPORT){
            timed_wake_switch.setChecked(timed_wake_switch_state !=
                    McPowerFlag.TIMED_TOUCH_WAKE_DISABLED? true:false);
        }
        manual_config_battery.setChecked(mcPower.getManualConfigBatteryState()==McPowerFlag.MANUAL_CONFIG_BATTERY_OPEN);
        battery_level_value.setText(""+mcPower.getBatteryLevel());
        battery_thread_value.setText(""+mcPower.getBatteryThresholdvalue());
        manual_config_battery_state_value = mcPower.getBatteryState()-11;
        battery_state_value.setSelection(manual_config_battery_state_value);
        battery_waring.setChecked(mcPower.getBatteryWarning()==McPowerFlag.MANUAL_CONFIG_BATTERY_WILL_WARING);
        initListener();
    }

    private void initListener() {
        btnReboot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int ret = mcPower.reboot();
                parseError(ret);
            }
        });

        btnDialogShutdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int ret = mcPower.shutdown(true);
                parseError(ret);
            }
        });

        btnShutdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int ret = mcPower.shutdown(false);
                parseError(ret);
            }
        });

        btnBrightness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int value = 0;
                try{
                    value = Integer.parseInt(brightnessvalue.getText().toString());
                }catch (Exception e){
                    ToastUtils.showShort(PowerActivity.this, "亮度值不规范");
                    return;
                }
                int ret = mcPower.setScreenBrightness(value);
                parseError(ret);
            }
        });

        btnTouchWake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int ret = mcPower.setTouchWake(touch_wake_state==McPowerFlag.TOUCH_WAKE_DISABLED);
                if(ret == McErrorCode.ENJOY_COMMON_ERROR_WRITE_SETTINGS_ERROR){
                    ToastUtils.showShort(PowerActivity.this, "触摸唤醒和定时触摸唤醒存在冲突，需要关闭定时触摸唤醒才能打开");
                    return;
                }
                touch_wake_state = (touch_wake_state==McPowerFlag.TOUCH_WAKE_DISABLED)?
                        McPowerFlag.TOUCH_WAKE_ENABLED:McPowerFlag.TOUCH_WAKE_DISABLED;
                btnTouchWake.setText((touch_wake_state==McPowerFlag.TOUCH_WAKE_DISABLED)?
                        "打开触摸唤醒":"关闭触摸唤醒");
                parseError(ret);
            }
        });

        btnTouchStatu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int ret = mcPower.getTouchWakeStatu();
                switch(ret){
                    case McPowerFlag.TOUCH_WAKE_DISABLED:{
                        ToastUtils.showShort(PowerActivity.this, "触摸唤醒已关闭");
                        break;
                    }
                    case McPowerFlag.TOUCH_WAKE_ENABLED:{
                        ToastUtils.showShort(PowerActivity.this, "触摸唤醒已打开");
                        break;
                    }
                    default:
                        parseError(ret);
                }
            }
        });

        timed_wake_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                int ret = mcPower.setTimedTouchWake(b);
                parseError(ret);
            }
        });

        add_time_touch_wake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String result = add_time_touch_wake_value.getText().toString().trim();
                String[] period = result.split("~");
                if(period.length < 3 || !period[0].contains(":") || !period[1].contains(":")){
                    ToastUtils.showShort(PowerActivity.this, "输入定时计划不规范！");
                    return;
                }
                try{
                    int ret = mcPower.addScheduleToTouchWake(period[0], period[1], Boolean.parseBoolean(period[2]));
                    switch(ret){
                        case McErrorCode.ENJOY_POWER_MANAGER_TIMED_TOUCH_WAKE_INFO_ERROR:
                            ToastUtils.showShort(PowerActivity.this, "定时触摸开始/结束时间格式不规范或者开始时间大于结束时间！");
                            return;
                        case McErrorCode.ENJOY_POWER_MANAGER_TIMED_TOUCH_WAKE_INFO_CONFLICT:
                            ToastUtils.showShort(PowerActivity.this, "时间存在冲突或者定时触摸唤醒总时间跨度超过一天");
                            return;
                    }
                    parseError(ret);
                }catch (Exception e){
                    ToastUtils.showShort(PowerActivity.this, "输入定时计划不规范！");
                }
            }
        });

        delete_time_touch_wake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String result = delete_time_touch_wake_value.getText().toString().trim();
                String[] period = result.split("~");
                if(period.length < 3 || !period[0].contains(":") || !period[1].contains(":")){
                    ToastUtils.showShort(PowerActivity.this, "输入定时计划不规范！");
                    return;
                }
                try{
                    int ret = mcPower.deleteScheduleToTouchWake(period[0], period[1], Boolean.parseBoolean(period[2]));
                    switch(ret){
                        case McErrorCode.ENJOY_POWER_MANAGER_TIMED_TOUCH_WAKE_INFO_ERROR:
                            ToastUtils.showShort(PowerActivity.this, "定时触摸开始/结束时间格式不规范");
                            return;
                        case McErrorCode.ENJOY_POWER_MANAGER_TIMED_TOUCH_WAKE_PLAN_NOT_EXITS:
                            ToastUtils.showShort(PowerActivity.this, "要删除的定时触摸唤醒时间段不存在");
                            return;
                    }
                    parseError(ret);
                }catch (Exception e){
                    ToastUtils.showShort(PowerActivity.this, "输入定时计划不规范！");
                }
            }
        });

        cancle_all_time_touch_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int ret = mcPower.cancleTimedTouchWake();
                parseError(ret);
            }
        });

        get_time_touch_switch_state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int ret = mcPower.getTimedTouchWakeState();
                switch (ret){
                    case McPowerFlag.TIMED_TOUCH_WAKE_DISABLED:{
                        ToastUtils.showShort(PowerActivity.this, "定时触摸唤醒已关闭");
                        break;
                    }
                    case McPowerFlag.TIMED_TOUCH_WAKE_OPEN_NOT_START:{
                        ToastUtils.showShort(PowerActivity.this, "定时触摸唤醒已打开，但未执行定时计划");
                        break;
                    }
                    case McPowerFlag.TIMED_TOUCH_WAKE_OPEN_AND_START:{
                        ToastUtils.showShort(PowerActivity.this, "定时触摸唤醒已打开，正在执行定时计划");
                        break;
                    }
                    default:parseError(ret);
                }
            }
        });

        get_time_touch_state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int ret = mcPower.getTimedTouchWakeEnable();
                switch (ret){
                    case McErrorCode.ENJOY_POWER_MANAGER_TIMED_TOUCH_WAKE_PLAN_NOT_START:{
                        ToastUtils.showShort(PowerActivity.this, "当前没有执行定时触摸唤醒计划");
                        break;
                    }
                    case McPowerFlag.TIMED_TOUCH_WAKE_PLAN_TOUCH_DISABELD:{
                        ToastUtils.showShort(PowerActivity.this, "执行定时触摸唤醒计划时触摸唤醒关闭");
                        break;
                    }
                    case McPowerFlag.TIMED_TOUCH_WAKE_PLAN_TOUCH_ENABLE:{
                        ToastUtils.showShort(PowerActivity.this, "执行定时触摸唤醒计划时触摸唤醒打开");
                        break;
                    }
                    default:parseError(ret);
                }
            }
        });

        manual_config_battery.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                int ret = mcPower.openManualConfigBattery(b);
                parseError(ret);
            }
        });

        battery_state_value.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                manual_config_battery_state_value = i+11;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        update_battery_level.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int result = mcPower.getBatteryLevel();
                try{
                    result = Integer.parseInt(battery_level_value.getText().toString());
                } catch (Exception e){
                    ToastUtils.showShort(PowerActivity.this, "电池电量信息填写不规范");
                    return;
                }
                int ret = mcPower.setBatteryLevel(result);
                switch (ret){
                    case McErrorCode.ENJOY_POWER_MANAGER_MANUAL_CONFIG_BATTERY_NOT_ALLOW:{
                        ToastUtils.showShort(PowerActivity.this, "主动配置电池开关未打开");
                        return;
                    }
                    case McErrorCode.ENJOY_POWER_MANAGER_BATTERY_LEVEL_INFO_ERROR:{
                        ToastUtils.showShort(PowerActivity.this, "电池电量信息不规范");
                        return;
                    }
                }
                parseError(ret);
            }
        });

        update_battery_state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int ret = mcPower.setBatteryState(manual_config_battery_state_value);
                switch (ret){
                    case McErrorCode.ENJOY_POWER_MANAGER_MANUAL_CONFIG_BATTERY_NOT_ALLOW:{
                        ToastUtils.showShort(PowerActivity.this, "主动配置电池开关未打开");
                        return;
                    }
                    case McErrorCode.ENJOY_POWER_MANAGER_BATTERY_STATE_INFO_ERROR:{
                        ToastUtils.showShort(PowerActivity.this, "电池充电状态不规范");
                        return;
                    }
                }
                parseError(ret);
            }
        });

        update_battery_thread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int result = mcPower.getBatteryThresholdvalue();
                try{
                    result = Integer.parseInt(battery_thread_value.getText().toString());
                } catch (Exception e){
                    ToastUtils.showShort(PowerActivity.this, "电池低电量阈值信息填写不规范");
                    return;
                }
                int ret = mcPower.setBatteryThresholdvalue(result);
                switch (ret){
                    case McErrorCode.ENJOY_POWER_MANAGER_MANUAL_CONFIG_BATTERY_NOT_ALLOW:{
                        ToastUtils.showShort(PowerActivity.this, "主动配置电池开关未打开");
                        return;
                    }
                    case McErrorCode.ENJOY_POWER_MANAGER_BATTERY_THRESHOLDVALUE_INFO_ERROR:{
                        ToastUtils.showShort(PowerActivity.this, "电池低电量阈值不规范");
                        return;
                    }
                }
                parseError(ret);
            }
        });

        update_battery_waring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean result = battery_waring.isChecked();
                int ret = mcPower.setBatteryWarning(result);
                switch (ret){
                    case McErrorCode.ENJOY_POWER_MANAGER_MANUAL_CONFIG_BATTERY_NOT_ALLOW:{
                        ToastUtils.showShort(PowerActivity.this, "主动配置电池开关未打开");
                        return;
                    }
                }
                parseError(ret);
            }
        });
    }

    private void parseError(int errorCode) {
        switch (errorCode) {
            case McErrorCode.ENJOY_COMMON_SUCCESSFUL:
                ToastUtils.showShort(PowerActivity.this, "成功");
                break;
            case McErrorCode.ENJOY_COMMON_ERROR_SERVICE_NOT_START:
                ToastUtils.showShort(PowerActivity.this, "电源服务错误："+McErrorCode.errorCode2Str(errorCode));
                break;
            default:
                ToastUtils.showShort(PowerActivity.this, "未知错误："+McErrorCode.errorCode2Str(errorCode));
                break;
        }
    }
}
