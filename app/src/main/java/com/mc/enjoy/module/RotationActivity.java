package com.mc.enjoy.module;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.mc.enjoy.R;
import com.mc.enjoy.ToastUtils;
import com.mc.enjoysdk.McPower;
import com.mc.enjoysdk.McRotation;
import com.mc.enjoysdk.result.McResultBool;
import com.mc.enjoysdk.transform.McErrorCode;

public class RotationActivity extends AppCompatActivity {
    private static final String TAG = "RotationActivity";

    private Button btnMain;
    private Button btnVice;
    private Button btnDisplay;
    private Spinner spinnerMain;
    private Spinner spinnerVice;
    private Spinner spinnerDisplay;
    private TextView system_rotation;
    private TextView vice_rotation;
    private TextView App_rotation;
    private CheckBox cbViceFull;
    private McRotation mcRotation;
    private McPower mcPower;
    private int curMainPos;
    private int curVicePos;
    private int curDisplayPos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rotation);

        btnMain = findViewById(R.id.btn_main);
        btnVice = findViewById(R.id.btn_vice);
        btnDisplay = findViewById(R.id.btn_display);
        spinnerMain = findViewById(R.id.spinner_main);
        spinnerVice = findViewById(R.id.spinner_vice);
        spinnerDisplay = findViewById(R.id.spinner_display);
        cbViceFull = findViewById(R.id.cb_vice_full);
        system_rotation = findViewById(R.id.system_rotation);
        vice_rotation = findViewById(R.id.vice_rotation);
        App_rotation = findViewById(R.id.App_rotation);

        mcRotation = McRotation.getInstance(this);
        mcPower = McPower.getInstance(this);
        cbViceFull.setChecked(mcRotation.isViceFull() == McResultBool.TRUE);
        initPos();
        initListener();
    }

    private void initPos() {
        curMainPos = mcRotation.getSystemRotation() / 90;
        curVicePos = mcRotation.getViceRotation() / 90;
        curDisplayPos = mcRotation.getDisplayRotation() / 90;
        // spinnerMain.setSelection 添加 boolean 参数可以解决 spiner 设置监听后自动触发 onItemSelected 事件
        spinnerMain.setSelection(curMainPos, false);
        spinnerVice.setSelection(curVicePos,false);
        spinnerDisplay.setSelection(curDisplayPos,false);
        system_rotation.setText("主屏幕角度："+(curMainPos*90));
        vice_rotation.setText("副屏幕角度："+(curVicePos*90));
        App_rotation.setText("App旋转角度："+(curDisplayPos*90));
    }

    private void initListener() {
        btnMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = spinnerMain.getSelectedItemPosition();
                if (position == curMainPos) {
                    return;
                }
                curMainPos = position;

                int ret = mcRotation.setSystemRotation(position * 90);
                parseError(ret);

                if (ret == McErrorCode.ENJOY_COMMON_SUCCESSFUL) {
                    new AlertDialog.Builder(RotationActivity.this)
                            .setTitle("是否重启查看主屏幕方向？")
                            .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .setPositiveButton("重启", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    mcPower.reboot();
                                }
                            })
                            .show();
                }
                initPos();
            }
        });

        btnVice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = spinnerVice.getSelectedItemPosition();
                if (position == curVicePos) {
                    return;
                }
                curVicePos = position;

                int ret = mcRotation.setViceRotation(position * 90);
                parseError(ret);
                initPos();
            }
        });

        btnDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = spinnerDisplay.getSelectedItemPosition();
                if (position == curDisplayPos) {
                    return;
                }
                curDisplayPos = position;

                int ret = mcRotation.setDisplayRotation(position * 90);
                parseError(ret);
                initPos();
            }
        });

        cbViceFull.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mcRotation.setViceFull(isChecked);
            }
        });
    }

    private String getRotation(int rotation) {
        switch (rotation) {
            case 0:
                return "横屏";
            case 90:
                return "竖屏";
            case 180:
                return "反向横屏";
            case 270:
                return "方向竖屏";
        }

        return "错误";
    }

    private void parseError(int errorCode) {
        switch (errorCode) {
            case McErrorCode.ENJOY_COMMON_SUCCESSFUL:
                ToastUtils.showShort(RotationActivity.this, "成功");
                break;
            case McErrorCode.ENJOY_COMMON_ERROR_SERVICE_NOT_START:
                ToastUtils.showShort(RotationActivity.this, "屏幕旋转服务未启动："+McErrorCode.errorCode2Str(errorCode));
                break;
            case McErrorCode.ENJOY_ROTATION_MANAGER_ERROR_DISPLAY_ROTATION:
                ToastUtils.showShort(RotationActivity.this, "主屏幕旋转错误："+McErrorCode.errorCode2Str(errorCode));
                break;
            case McErrorCode.ENJOY_ROTATION_MANAGER_ERROR_SYSTEM_ROTATION:
                ToastUtils.showShort(RotationActivity.this, "临时转屏错误："+McErrorCode.errorCode2Str(errorCode));
                break;
            case McErrorCode.ENJOY_ROTATION_MANAGER_ERROR_VICE_ROTATION:
                ToastUtils.showShort(RotationActivity.this, "副屏旋转错误："+McErrorCode.errorCode2Str(errorCode));
                break;
            case McErrorCode.ENJOY_ROTATION_MANAGER_ERROR_VICE_FULL:
                ToastUtils.showShort(RotationActivity.this, "副屏幕全屏错误："+McErrorCode.errorCode2Str(errorCode));
                break;
            case McErrorCode.ENJOY_ROTATION_MANAGER_ERROR_ROTATION_FORMAT:
                ToastUtils.showShort(RotationActivity.this, "旋转角度参数格式错误："+McErrorCode.errorCode2Str(errorCode));
                break;
            default:
                ToastUtils.showShort(RotationActivity.this, "未知错误："+McErrorCode.errorCode2Str(errorCode));
                break;
        }
    }
}