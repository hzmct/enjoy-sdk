package com.mc.enjoy.module;

import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.mc.android.mcinstall.McWhiteApp;
import com.mc.enjoy.AppUtils;
import com.mc.enjoy.R;
import com.mc.enjoy.ToastUtils;
import com.mc.enjoysdk.McInstall;
import com.mc.enjoysdk.result.McResultBool;
import com.mc.enjoysdk.transform.McErrorCode;

import java.util.ArrayList;

public class WhiteAppActivity extends AppCompatActivity {
    private static final String TAG = "WhiteAppActivity";

    private CheckBox cbEnable;
    private EditText etPackageName;
    private RadioButton rbForeground;
    private RadioButton rbBackground;
    private CheckBox cbAllowUnistall;
    private CheckBox cbOpenAfterInstall;
    private CheckBox cbOpenFuzzyInstall;
    private Button btnAdd;
    private Button btnRemove;
    private Button btnWhiteList;
    private TextView tvWhiteList;

    private McInstall mcInstall;
    private ArrayList<McWhiteApp> mcWhiteApps = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_white_app);

        cbEnable = findViewById(R.id.cb_enable);
        etPackageName = findViewById(R.id.et_package_name);
        rbForeground = findViewById(R.id.rb_install_foreground);
        rbBackground = findViewById(R.id.rb_install_background);
        cbAllowUnistall = findViewById(R.id.cb_allow_uninstall);
        cbOpenAfterInstall = findViewById(R.id.cb_open_after_install);
        cbOpenFuzzyInstall = findViewById(R.id.cb_open_fuzzy_install);
        btnAdd = findViewById(R.id.btn_add);
        btnRemove = findViewById(R.id.btn_remove);
        btnWhiteList = findViewById(R.id.btn_white_list);
        tvWhiteList = findViewById(R.id.tv_white_list);

        tvWhiteList.setMovementMethod(ScrollingMovementMethod.getInstance());

        initData();
        initListener();
    }

    private void initData() {
        mcInstall = McInstall.getInstance(this);
        Log.i(TAG, "whiteApps isEnable == " + mcInstall.isEnable());
        cbEnable.setChecked(mcInstall.isEnable() == McResultBool.TRUE);

        Log.i(TAG, "this is allowInstall == " + mcInstall.isInstallAllow(AppUtils.getAppPackageName(this))
                + ", isAllowUninstall == " + mcInstall.isUninstallAllow(AppUtils.getAppPackageName(this))
                + ", isOpenAfterInstall == " + mcInstall.isOpenAfterInstall(AppUtils.getAppPackageName(this))
                + ", installMode == " + mcInstall.getInstallMode(AppUtils.getAppPackageName(this)));

        getWhiteApps();
    }

    private void initListener() {
        cbEnable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int ret = mcInstall.whiteListSwitch(isChecked);
                parseError(ret);
            }
        });

        rbForeground.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                cbOpenAfterInstall.setEnabled(!b);
                cbOpenAfterInstall.setChecked(false);
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etPackageName.getText().toString().trim())) {
                    ToastUtils.showShort(WhiteAppActivity.this, "白名单应用包名不能为空");
                    return;
                }

                if (!rbForeground.isChecked() && !rbBackground.isChecked()) {
                    ToastUtils.showShort(WhiteAppActivity.this, "白名单应用安装模式不能为空");
                    return;
                }

                ArrayList<McWhiteApp> mcWhiteApps = new ArrayList<>();
                McWhiteApp app = new McWhiteApp();
                app.setPackageName(etPackageName.getText().toString().trim());
                if (rbForeground.isChecked()) {
                    app.setInstallMode(McWhiteApp.INSTALL_MODE_FOREGROUND);
                } else if (rbBackground.isChecked()) {
                    app.setInstallMode(McWhiteApp.INSTALL_MODE_BACKGROUND);
                }
                app.setAllowUninstall(cbAllowUnistall.isChecked());
                app.setOpenAfterInstall(cbOpenAfterInstall.isChecked());
                app.setOpenFuzzyInstall(cbOpenFuzzyInstall.isChecked());
                mcWhiteApps.add(app);

                int ret = mcInstall.addWhiteList(mcWhiteApps);
                parseError(ret);
                getWhiteApps();
            }
        });

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etPackageName.getText().toString().trim())) {
                    ToastUtils.showShort(WhiteAppActivity.this, "白名单应用包名不能为空");
                    return;
                }

                ArrayList<McWhiteApp> mcWhiteApps = new ArrayList<>();
                McWhiteApp app = new McWhiteApp();
                app.setPackageName(etPackageName.getText().toString().trim());
                mcWhiteApps.add(app);

                int ret = mcInstall.removeWhiteList(mcWhiteApps);
                parseError(ret);
                getWhiteApps();
            }
        });

        btnWhiteList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWhiteApps();
            }
        });
    }

    private void getWhiteApps() {
        mcWhiteApps.clear();
        mcWhiteApps.addAll(mcInstall.getWhiteList());

        StringBuilder s = new StringBuilder();
        for (McWhiteApp app : mcWhiteApps) {
            s.append("包名: " + app.getPackageName() + "; ");
            s.append("安装方式: " + (app.getInstallMode() == 0 ? "前台安装; " : "后台安装; "));
            s.append("允许卸载: " + (app.isAllowUninstall() ? "是; " : "否; "));
            s.append("安装完自启动: " + (app.isOpenAfterInstall() ? "是; " : "否; "));
            s.append("包名支持模糊匹配: " + (app.isOpenFuzzyInstall() ? "是; " : "否; "));
            s.append("\n");
        }

        tvWhiteList.setText(s.toString());
    }

    private void parseError(int errorCode) {
        switch (errorCode) {
            case McErrorCode.ENJOY_COMMON_SUCCESSFUL:
                ToastUtils.showShort(WhiteAppActivity.this, "成功");
                break;
            case McErrorCode.ENJOY_COMMON_ERROR_SERVICE_NOT_START:
                ToastUtils.showShort(WhiteAppActivity.this, "应用白名单服务未启动："+McErrorCode.errorCode2Str(errorCode));
                break;
            case McErrorCode.ENJOY_INSTALL_MANAGER_ERROR_WHITE_ADD:
                ToastUtils.showShort(WhiteAppActivity.this, "添加白名单失败："+McErrorCode.errorCode2Str(errorCode));
                break;
            case McErrorCode.ENJOY_INSTALL_MANAGER_ERROR_WHITE_REMOVE:
                ToastUtils.showShort(WhiteAppActivity.this, "从白名单中移除失败："+McErrorCode.errorCode2Str(errorCode));
                break;
            default:
                ToastUtils.showShort(WhiteAppActivity.this, "未知错误："+McErrorCode.errorCode2Str(errorCode));
                break;
        }
    }
}