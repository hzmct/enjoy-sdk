package com.mc.enjoy;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.mc.enjoy.bean.EnjoyBean;
import com.mc.enjoy.constant.EnjoyEnum;
import com.mc.enjoy.module.BootAnimationActivity;
import com.mc.enjoy.module.EthActivity;
import com.mc.enjoy.module.EthTetherActivity;
import com.mc.enjoy.module.FirmwareActivity;
import com.mc.enjoy.module.HardwareKeyBoardActivity;
import com.mc.enjoy.module.HardwareStatusActivity;
import com.mc.enjoy.module.HomeActivity;
import com.mc.enjoy.module.KeepAliveActivity;
import com.mc.enjoy.module.NetCoexistenceActivity;
import com.mc.enjoy.module.PowerActivity;
import com.mc.enjoy.module.RotationActivity;
import com.mc.enjoy.module.SecureActivity;
import com.mc.enjoy.module.SystemUiActivity;
import com.mc.enjoy.module.TimeActivity;
import com.mc.enjoy.module.WatchDogActivity;
import com.mc.enjoy.module.WhiteAppActivity;
import com.mc.enjoy.permission.PermissionUtil;
import com.mc.enjoysdk.McSecure;
import com.mc.enjoysdk.result.McResultBool;
import com.mc.enjoysdk.transform.McEnjoySdkInfo;
import com.mc.enjoysdk.transform.McSecurePasswordState;

import java.util.ArrayList;
import java.util.List;

import static com.mc.enjoy.constant.EnjoyEnum.BOOT_ANIMATION;
import static com.mc.enjoy.constant.EnjoyEnum.ETHERNET;
import static com.mc.enjoy.constant.EnjoyEnum.ETH_TETHER;
import static com.mc.enjoy.constant.EnjoyEnum.FIRMWARE_INFO;
import static com.mc.enjoy.constant.EnjoyEnum.HARDWARE_KEYBOARD;
import static com.mc.enjoy.constant.EnjoyEnum.HARDWARE_STATUS;
import static com.mc.enjoy.constant.EnjoyEnum.HOME;
import static com.mc.enjoy.constant.EnjoyEnum.INSTALL;
import static com.mc.enjoy.constant.EnjoyEnum.KEEPALIVE;
import static com.mc.enjoy.constant.EnjoyEnum.NET_COEXIST;
import static com.mc.enjoy.constant.EnjoyEnum.POWER;
import static com.mc.enjoy.constant.EnjoyEnum.ROTATION;
import static com.mc.enjoy.constant.EnjoyEnum.SECURE;
import static com.mc.enjoy.constant.EnjoyEnum.SYSTEM_UI;
import static com.mc.enjoy.constant.EnjoyEnum.TIME;
import static com.mc.enjoy.constant.EnjoyEnum.WATCH_DOG;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private static final String TAG = "MainActivity";
    private int RESULT_OK = 100;
    private EnjoyEnum enjoyEnum = SECURE;

    private ListView listView;
    private EnjoyAdapter enjoyAdapter;
    private TextView tvEnjoySdkVersion;
    private TextView tvEnjoyVersion;
    private TextView tvEnjoyCompatibleAndroidSdk;

    private ArrayList<EnjoyBean> enjoyBeans = new ArrayList<>();
    private PermissionUtil permissionUtil = new PermissionUtil();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        注册为安全应用后，无需动态权限申请
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !permissionUtil.hasPermission(this)) {
//            permissionUtil.requestPermission(this);
//        }

        listView = findViewById(R.id.recycler_view);
        tvEnjoySdkVersion = findViewById(R.id.tv_enjoy_sdk_version);
        tvEnjoyVersion = findViewById(R.id.tv_enjoy_version);
        tvEnjoyCompatibleAndroidSdk = findViewById(R.id.tv_enjoy_compatible_android_sdk);

        initData();
//        新的 McSecure 注销时会收回注册时授予的应用权限，导致了 APP Gid 发生变更，系统对 APP 进行 Kill
//        initSecure();
        initEnjoyVersion();
    }

    private void initData() {
        EnjoyBean bootAnimationBean = new EnjoyBean(BOOT_ANIMATION, "开机动画测试");
        EnjoyBean ethernetBean = new EnjoyBean(ETHERNET, "以太网配置测试");
        EnjoyBean ethTetherBean = new EnjoyBean(ETH_TETHER, "以太网共享测试");
        EnjoyBean firmwareInfoBean = new EnjoyBean(FIRMWARE_INFO, "固件信息测试");
        EnjoyBean hardwareKeyBoardBean = new EnjoyBean(HARDWARE_KEYBOARD, "硬件输入设备测试");
        EnjoyBean hardwareStatusBean = new EnjoyBean(HARDWARE_STATUS, "硬件状态测试");
        EnjoyBean homeBean = new EnjoyBean(HOME, "开机程序测试");
        EnjoyBean installBean = new EnjoyBean(INSTALL, "应用白名单测试");
        EnjoyBean keepaliveBean = new EnjoyBean(KEEPALIVE, "应用保活测试");
        EnjoyBean netCoexistBean = new EnjoyBean(NET_COEXIST, "三网共存测试");
        EnjoyBean powerBean = new EnjoyBean(POWER, "电源测试");
        EnjoyBean rotationBean = new EnjoyBean(ROTATION, "屏幕旋转测试");
        EnjoyBean secureBean = new EnjoyBean(SECURE, "权限密码测试");
        EnjoyBean systemUiBean = new EnjoyBean(SYSTEM_UI, "系统界面测试");
        EnjoyBean timeBean = new EnjoyBean(TIME, "时间测试");
        EnjoyBean watchDogBean = new EnjoyBean(WATCH_DOG, "看门狗测试");

        enjoyBeans.add(bootAnimationBean);
        enjoyBeans.add(ethernetBean);
        enjoyBeans.add(ethTetherBean);
        enjoyBeans.add(firmwareInfoBean);
        enjoyBeans.add(hardwareKeyBoardBean);
        enjoyBeans.add(hardwareStatusBean);
        enjoyBeans.add(homeBean);
        enjoyBeans.add(installBean);
        enjoyBeans.add(keepaliveBean);
        enjoyBeans.add(netCoexistBean);
        enjoyBeans.add(powerBean);
        enjoyBeans.add(rotationBean);
        enjoyBeans.add(secureBean);
        enjoyBeans.add(systemUiBean);
        enjoyBeans.add(timeBean);
        enjoyBeans.add(watchDogBean);

        enjoyAdapter = new EnjoyAdapter(this, R.layout.item_enjoy, enjoyBeans);
        listView.setAdapter(enjoyAdapter);
        listView.setOnItemClickListener(this);
    }

    private void initSecure() {
        McSecure mcSecure = McSecure.getInstance(this);
        int ret = mcSecure.getSecurePasswdStatus();
        if (ret == McSecurePasswordState.MC_SECURE_PASSWD_EMPTY){
            ret = mcSecure.setSecurePasswd(null,"Abc12345");
            Log.i(TAG, "setSecurePasswd == " + ret);
        }
        mcSecure.registSafeProgram("Abc12345");
    }

    @SuppressLint("StringFormatMatches")
    private void initEnjoyVersion() {
        tvEnjoySdkVersion.setText(String.format(getString(R.string.enjoy_sdk_version, McEnjoySdkInfo.SDK_VERSION)));
        tvEnjoyVersion.setText(String.format(getString(R.string.enjoy_version, McEnjoySdkInfo.VERSION)));
        tvEnjoyCompatibleAndroidSdk.setText(String.format(getString(R.string.enjoy_android_sdk, McEnjoySdkInfo.COMPATIBLE_ANDROID_SDK)));
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        EnjoyBean enjoyBean = (EnjoyBean)adapterView.getItemAtPosition(i);

        //优先检测当前应用是否为安全应用
        McSecure mcSecure = McSecure.getInstance(this);
        McResultBool ret = mcSecure.checkSafeProgramOfSelf();
        if (ret != McResultBool.TRUE && enjoyBean.getId() != SECURE){
            ToastUtils.showShort(MainActivity.this, "该应用未注册为安全应用");
            Intent intent = new Intent(MainActivity.this, SecureActivity.class);
            intent.putExtra("first_to_register",true);
            enjoyEnum = enjoyBean.getId();
            startActivityForResult(intent, 1000);
            return;
        }

        StartEnjoyActivity(enjoyBean.getId());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1000 && resultCode==RESULT_OK){
            StartEnjoyActivity(enjoyEnum);
        }
    }

    private void StartEnjoyActivity(EnjoyEnum enjoyEnum){
        switch (enjoyEnum){
            case BOOT_ANIMATION:
                startActivity(new Intent(MainActivity.this, BootAnimationActivity.class));
                break;
            case ETHERNET:
                startActivity(new Intent(MainActivity.this, EthActivity.class));
                break;
            case ETH_TETHER:
                startActivity(new Intent(MainActivity.this, EthTetherActivity.class));
                break;
            case FIRMWARE_INFO:
                startActivity(new Intent(MainActivity.this, FirmwareActivity.class));
                break;
            case HARDWARE_KEYBOARD:
                startActivity(new Intent(MainActivity.this, HardwareKeyBoardActivity.class));
                break;
            case HARDWARE_STATUS:
                startActivity(new Intent(MainActivity.this, HardwareStatusActivity.class));
                break;
            case HOME:
                startActivity(new Intent(MainActivity.this, HomeActivity.class));
                break;
            case INSTALL:
                startActivity(new Intent(MainActivity.this, WhiteAppActivity.class));
                break;
            case KEEPALIVE:
                startActivity(new Intent(MainActivity.this, KeepAliveActivity.class));
                break;
            case NET_COEXIST:
                startActivity(new Intent(MainActivity.this, NetCoexistenceActivity.class));
                break;
            case POWER:
                startActivity(new Intent(MainActivity.this, PowerActivity.class));
                break;
            case ROTATION:
                startActivity(new Intent(MainActivity.this, RotationActivity.class));
                break;
            case SECURE:
                startActivity(new Intent(MainActivity.this, SecureActivity.class));
                break;
            case SYSTEM_UI:
                startActivity(new Intent(MainActivity.this, SystemUiActivity.class));
                break;
            case TIME:
                startActivity(new Intent(MainActivity.this, TimeActivity.class));
                break;
            case WATCH_DOG:
                startActivity(new Intent(MainActivity.this, WatchDogActivity.class));
                break;
        }
    }

    private class EnjoyAdapter extends ArrayAdapter<EnjoyBean> {
        private int resourceId;
        public EnjoyAdapter(@NonNull Context context, int resource, @NonNull List<EnjoyBean> objects) {
            super(context, resource, objects);
            resourceId = resource;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            EnjoyBean enjoyBean= getItem(position);
            View view;
            ViewHolder viewHolder;
            if (convertView == null){
                view = LayoutInflater.from(getContext()).inflate(resourceId,parent,false);
                viewHolder = new ViewHolder();
                viewHolder.moduleTitle = (TextView) view.findViewById(R.id.tv_enjoy);
                view.setTag(viewHolder);
            }else{
                view = convertView;
                viewHolder = (ViewHolder) view.getTag();
            }
            viewHolder.moduleTitle.setText(enjoyBean.getName());
            return view;
        }

        class ViewHolder{
            TextView moduleTitle;
        }
    }
}